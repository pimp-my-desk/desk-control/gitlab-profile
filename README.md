# Desk Control

Here you can find projects related to retrofitting "Smart" functionalities in "Dumb" electric sit/stand desks.

## [Jiecang Reverse Engineering](https://gitlab.com/pimp-my-desk/desk-control/jiecang-reverse-engineering)

A compilation of information on Jiecang desks that will include most or all of the protocol via RJ-12 port.

## [Desky Standing Desk (ESPHome) [Works with Desky, Uplift, Jiecang, Assmann & others]](https://community.home-assistant.io/t/desky-standing-desk-esphome-works-with-desky-uplift-jiecang-assmann-others)
A thread with a lot of experience from users of this projects.

## [IoT Jarvis standup desk interface](https://github.com/phord/Jarvis)

A project for ESP8266 with MQTT, it acts as a man in the middle, makes use of the RJ-45 port but has info on the RJ-12 port.
This project has most of the info about Jiecang's desk protocol. 

## [Upsy Desky](https://github.com/tjhorner/upsy-desky)
A project that includes files to build everything and connect your desk to your automation system, also makes use of the RJ-45 port

## [ESPHome Jiecang Desk Controller](https://github.com/Rocka84/jiecang_desk_controller)
A project that makes use of the ESPHome to control the desk.